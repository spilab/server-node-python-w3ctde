from owlready2 import *
import yaml
import json
import subprocess
import datetime
import rdflib
import time
import os
from subprocess import call

# Times distance


# Locations distance
import geopy.distance

# Compare arrays
import collections

# Dependencies
# sudo pip3 install Owlready2
# sudo pip3 install pyyaml
# sudo pip3 install yeelight
# sudo pip3 install rdflib

#ontoName = "./complete1.owl"
#ontoName = "./ontologies/skeleton.owl"
ontoName = "./ontologies/skeleton-modified.owl"
#ontoName = "http://www.lesfleursdunormal.fr/static/_downloads/pizza_onto.owl"
#ontoName = "http://homepages.laas.fr/nseydoux/ontologies/IoT-O.owl"
#ontoName = "http://iot.ee.surrey.ac.uk/fiware/ontologies/iot-lite/iot-lite.rdf"
#ontoName = "http://sensormeasurement.appspot.com/m3.owl"

onto_path.append("./ontologies/")
#onto = get_ontology("./complete1.owl")
#onto = get_ontology("http://www.lesfleursdunormal.fr/static/_downloads/pizza_onto.owl")
#onto = get_ontology("./skeleton.owl")
#onto = get_ontology("http://homepages.laas.fr/nseydoux/ontologies/IoT-O.owl") # Error 
#onto = get_ontology("http://iot.ee.surrey.ac.uk/fiware/ontologies/iot-lite/iot-lite.rdf")

onto = get_ontology(ontoName) 
onto.load()
#print(onto)

sentSituation = True
logData = ""
start = ""
end = ""

##########################
## Situation parameters ##
##########################
curLoc = "39.4788261,-6.3423754" # Location of the environment (Spilab)
distLoc = 50 # Threshold (in meters) to consider that is a similar location with a situation
distTime = 15 # Threshold (in minutes) to consider that is a similar time with a situation
###########################

#print("onto=",onto.iri)

class Entity(Thing):
	namespace = onto

#class Service(Thing):
#	namespace = onto

#class Operation(Thing):
#	namespace = onto

class Objective(Thing):
	namespace = onto

class Action(Thing):
	namespace = onto

class Situation(Thing):
	namespace = onto

class Property(Thing):
	namespace = onto

class Strategy(Thing):
	namespace = onto

class Item(Thing):
	namespace = onto
	
def loadOntology():
	
	#onto.load()

	print ("Ontology loaded succesfully")
	return
	
def loadOntologyRDFlib():
	my_world = World()
	onto = my_world.get_ontology(ontoName).load()
	graph = my_world.as_rdflib_graph()
	return
	
def parseJSON(fileName):
	print ("\nReading:",str(fileName))
	print ("---------------")
	
	global logData
	
	file = open(fileName, "r")

	data = ""

	#showOntology()

	if file:
		data = json.load(file)
		
		# Create the entity
		
		eName = data["id"]
		e = Entity(eName)

		try:
			destroy_entity(onto[data["id"]])
		except:
			pass
		
		#showOntology()
		

		eName = data["id"]
		e = onto[eName]

		if e is None:
			e = Entity(eName)

		eName = data["id"]
		e = onto[eName]
		
		# Get info
		e.hasId.append(data["id"])
		#e.hasName.append(data["hasName"])
		#e.IPAddress.append(data["IPAddress"])
		e.macAddress.append(data["macAddress"])
		
		
		logData = logData + data["id"] + ";" + data["macAddress"]
		print(logData)
		print("\n")

		#logData = logData + data["hasId"] + "-" + data["hasName"] + "-" + data["macAddress"]
		
		

		# Get Objectives (Goals)
		objectives = data["objectives"]
		for oo in objectives:
			for o in oo.items():
				#print (oo[0])
				
				objId = o[0]
				#objName = o[1]["name"]
				objValue = o[1]["value"]

				# Create the objective in the ontology
				try:
					destroy_entity(onto[id])
				except:
					pass
				obj = Objective(objId)

				# Set objective properties
				onto[objId].powerValue.append(objValue)

				# Associate the objective with the entity
				onto[eName].hasObjective.append(obj)
	
		
		# Get Actions (Skills)
		actions = data["actions"]
		for aa in actions:
			for a in aa.items():
				actId = a[0]

				actForms = a[1]["forms"][0]["href"]

				# Create the action in the ontology
				try:
					destroy_entity(onto[actId])
				except:
					pass
				act = Action(actId)
							
				# Set ation properties
				onto[actId].forms.append(actForms)

				# Associate the action with the entity
				onto[eName].hasAction.append(act)

		# Get Situations
		situations = data["situations"]
		for ss in situations:
			for s in ss.items():
				#print("S=",s[1]["id"])

				sitId = s[1]["id"]

				#print (sitId)
				#print("\n")

				
				# Create the situation
				try:
					destroy_entity(onto["sit_"+sitId])
					#print("Destroy situ")
				except:
					pass
				sit = Situation("sit_"+sitId)

				# Situation properties
				properties = s[1]["properties"]
				for p in properties.items():
					#pr = p[1]
					propName = "sit_"+sitId+"-"+p[0]

					try:
						destroy_entity(onto[propName])
					except:
						pass

					sit = Situation("sit_"+sitId)

					prop = Property(propName)
					prop.realStateValue.append(p[1])
					sit.hasProperty.append(prop)
			
				# Situation objectives (items)
				objectives = s[1]["objectives"]["items"]
				for o in objectives:	
					sitObj = "sit_"+sitId+"_obj"+"-"+o["objective"]

					try:
						destroy_entity(onto[sitObj])
					except:
						pass

					it = Item(sitObj)
					it.itemThing.append(o["thing"])
					it.itemObjective.append(o["objective"])
					it.itemValue.append(o["value"])
					sit.hasObjective.append(it)
				
				# Situation things (entities)
				things = s[1]["things"]["items"]
				for t in things:
					e = Entity(t)
					sit.hasEntity.append(e)
				
				# Situation strategy
				sitStr = "sit_"+sitId+"_str"
				strategies = s[1]["strategy"]["items"]
				for s in strategies:	
					sitStr = "sit_"+sitId+"_str"+"-"+s["action"]

					try:
						destroy_entity(onto[sitStr])
					except:
						pass

					it = Item(sitStr)
					it.itemThing.append(s["thing"])
					it.itemObjective.append(s["action"])
					it.itemValue.append(s["value"])
					sit.hasStrategy.append(it)

				# Add situation to the entity
				onto[eName].hasSituation.append(sit)

		# Save ontology		
		onto.save()
		#onto.save(file = "./ontologies/skeleton-modified.owl", format = "rdfxml")
		
		# Set status to 1 (present in the environment)
		editPresentThings(eName, 1)

		
	return data

def checkEntitySituationWithObjectives(data):
	print ("\n")
	print ("0. Current state respect the objectives and entities")
	print ("----------------------------------------------------")

	# Current objectives
	curObjectives = data["objectives"]
	curObjectivesConverted = []
	for oo in curObjectives:
		for o in oo.items():
			obj = {}
			obj["thing"] = data["id"]
			obj["objective"] = o[0]
			obj["value"] = o[1]["value"]
			curObjectivesConverted.append(obj)

	# Stored situations
	situations = data["situations"]
	for ss in situations:
		for s in ss.items():
			#print("Situation=",s)
			sitObjectives = s[1]["objectives"]["items"]
			print("Situation =",s[1]["id"])
			#print("Objectives =",sitObjectives)
			for o in sitObjectives:
				print("  - Objective = ", o["thing"] + " - " + o["objective"] + " - " + o["value"])

			#same = collections.Counter(curObjectives) == collections.Counter(sitObjectives)


			print(curObjectivesConverted)
			print("\n")
			print(sitObjectives)


			same = isSubArray(sitObjectives, curObjectivesConverted, len(sitObjectives), len(curObjectivesConverted))
			print("Same =",same)

			if (curObjectivesConverted == sitObjectives):
				print("Objetivos IGUALES!")
			else:
				print("Objetivos DIFERENTES!")

	return 

def isSubArray(A, B, n, m): 
      
    # Two pointers to traverse the arrays 
    i = 0; j = 0; 
  
    # Traverse both arrays simultaneously 
    while (i < n and j < m): 
  
        # If element matches 
        # increment both pointers 
        if (A[i] == B[j]): 
  
            i += 1; 
            j += 1; 
  
            # If array B is completely 
            # traversed 
            if (j == m): 
                return True; 
          
        # If not, 
        # increment i and reset j 
        else: 
            i = i - j + 1; 
            j = 0; 
          
    return False; 


def checkCurrentStateWithObjectives(data):

	print ("\n")
	print ("TESTTT 0. Current state respect the objectives and entities")
	print ("----------------------------------------------------")

	objId = ""
	objValue = ""
	aValue = ""
	modifyActions = False

	objectives = data["objectives"]
	for oo in objectives:
		for o in oo.items():
			objId = o[0]
			objValue = o[1]["value"]
			aName = str(objId).replace("g_","sk_")
			aEntity = onto[aName]		
			if aEntity:
				aValue = onto[aName].realStateValue[len(onto[aName].realStateValue)-1]
				if objValue != aValue:
					modifyActions = True		
			else:
				aValue = "Unknown"
			print (" - Objective =",objId,"-",objValue,"->",aValue)
	print("\n --> Must modify? ->",modifyActions)

	

	"""
	individuals = list(onto.individuals())
	for i in individuals:
		if str(type(i)) == str(onto.name)+".Entity":
			print("I=",i)
	"""

	return
	
def parseYaml():
	print ("\nCustomize.yaml")
	print ("---------------")
	stream = open("customize.yaml", "r")
	docs = yaml.load_all(stream)
	for doc in docs:
		for e,v in doc.items():
			# Get entity name
			print ("\nEntity :",e)
			
			# Check if present
			p = v.get("E_Present")
			
			if p:
			
				print (" - Present:",p)
				
				
				# Create the individual
				entity = Entity(e)
			
				# Get entity skills
				skills = v.get("skills")
				sJson = json.dumps(skills)
				ss = json.loads(sJson)
				print (" - Services: ")
				if sJson != "null":
					for key, value in ss.items():
						# Create the service
						#print ("    * ",key,"->", value)
						#print ("	* key=",key)
						
						sk_name = key
						sk_CurValue = value.get("sk_CurValue")
						sk_EndPoint = value.get("hasAddress")
						if sk_name != "" and sk_CurValue != "" and sk_EndPoint != "":		
							service = Service(sk_name)
							# Set service properties
							onto[sk_name].sk_CurValue.append(sk_CurValue)
							onto[sk_name].hasAddress.append(sk_EndPoint)
							# Associate the service with the entity
							onto[e].hasService.append(service)
						
				#print ("\t",skills)
				
				# Get entity goals
				goals = v.get("goals")		
				gJson = json.dumps(goals)
				gg = json.loads(gJson)
				print (" - Operations: ")
				if gJson != "null":
					for key, value in gg.items():
						# Create operation
						print ("    * ",key,"->", value)
						operation = Operation(key)
						
						# 
						destroy_entity(onto[key])
						
						# Associate the operation with the entity					
						onto[key].powerValue.append(value)
						onto[e].hasOperation.append(operation)
						
				#print ("\t",goals)
		print ("\n")

def checkSituations(data):
	print ("\n")
	print ("Checking Situations")
	print ("-------------------")

	# Cheking situations. Check the current entities in the ontology with all the situations
	# When an existing situation is detected:
	# 1.- Same entities (present entities in the environment and in the situation)
	# 2.- Contextual properties
	# 2.1- Localization (approx location)+/- 50 meters
	# 2.2- Date (same date)
	# 2.3- Time (approx time)+/- 30 min

	# Get entity's current objectives
	curObjectives = data["objectives"]
	curObjectivesConverted = []
	for oo in curObjectives:
		for o in oo.items():
			obj = {}
			obj["thing"] = data["id"]
			obj["objective"] = o[0]
			obj["value"] = o[1]["value"]
			curObjectivesConverted.append(obj)

	# Get current Entities (from file)
	presentThings = readPresentThings()

	# Get entity situations
	#print("Entity situations: ")
	# Get Situations

	situationFound = False
	situations = data["situations"]
	for ss in situations:
		for s in ss.items():

			if not situationFound:

				sitProperties = []
				sitObjectives = []
				sitThings = []
				sitStrategies = []

				thingFlag = False
				locationFlag = False
				dateFlag = False
				timeFlag = False
				objectivesFlag = False

				distancelocation = ""
				distanceDate = ""
				distanceTime = ""

				sitId = s[1]["id"]

				# Situation properties
				properties = s[1]["properties"]
				for p in properties.items():
					prop = p[0]
					if prop == "location":
						sitLoc = p[1]
						#print(p[1])

						# Distance between locations

						#print(sitLoc.split(",")[0])

						d1 = (sitLoc.split(",")[0],sitLoc.split(",")[1])
						d2 = (curLoc.split(",")[0],curLoc.split(",")[1])
						distancelocation = round(geopy.distance.distance(d1, d2).m)

						#print("Distance location =", distancelocation, "meters")
						if distancelocation <= distLoc:
							locationFlag = True

					elif prop == "date":
						curDate = datetime.datetime.now().date()
						sitDate = p[1]
						sitDate = datetime.datetime.strptime(sitDate,"%Y-%m-%d").date()

						distanceDate = abs((curDate - sitDate).days)

						#print("Distance date =", distanceDate, "days")

						if (distanceDate == 0):
							dateFlag = True
					
					elif prop == "time":
						now = datetime.datetime.now()
						now = now.strftime("%H:%M:%S")
						curTime = datetime.datetime.strptime(now,"%H:%M:%S")

						sitTime = p[1]
						sitTime = datetime.datetime.strptime(sitTime,"%H:%M:%S")

						#print("CT=",curTime)
						#print("ST=",sitTime)

						distanceTime = abs((sitTime - curTime))
						distanceTime = round(distanceTime.total_seconds() / 60.0)

						#print("Distance time =", distanceTime, "minutes")

						if distanceTime <= distTime:
							timeFlag = True			

					sitProperties.append(p)
				
				# Situation objectives (items)
				objectives = s[1]["objectives"]["items"]
				for o in objectives:	
					sitObjectives.append(o)
					#print("  - Objective = ", o["thing"] + " - " + o["objective"] + " - " + o["value"])

				
			
				# Situation things (entities)
				things = s[1]["things"]["items"]
				for t in things:
					sitThings.append(t)

				

				# Situation strategy
				strategies = s[1]["strategy"]["items"]
				for s in strategies:	
					sitStrategies.append(s)

				#print(sitProperties)
				#print(sitObjectives)
				#print(sitThings)
				#print(sitStrategies)

				same = collections.Counter(	presentThings) == collections.Counter(sitThings)
				if (same):
					thingFlag = True

				###      TESTING     ###
				### REMOVE WHEN COMPLETE
				"""
				thingFlag = True
				locationFlag = True
				dateFlag = True
				timeFlag = True
				"""
				#thingFlag = True
				########################

				###

				"""
				# Stored situations
				sitObjectives = s[1]["objectives"]["items"]
				print("Situation =",sitId)
				#print("Objectives =",sitObjectives)
				for o in sitObjectives:
					print("  - Objective = ", o["thing"] + " - " + o["objective"] + " - " + o["value"])

				"""

				#same = collections.Counter(curObjectives) == collections.Counter(sitObjectives)


				#print(curObjectivesConverted)
				#print("\n")
				#print(sitObjectives)


				same = isSubArray(sitObjectives, curObjectivesConverted, len(sitObjectives), len(curObjectivesConverted))
				#print("Same =",same)

				if (curObjectivesConverted == sitObjectives):
				#	print("Objetivos IGUALES!")
					objectivesFlag = True
				#else:
				#	print("Objetivos DIFERENTES!")
				###




				if thingFlag and objectivesFlag and locationFlag and dateFlag and timeFlag:
					print("Situation=",sitId+" *** FOUND! ***")
				else:
					print("Situation=",sitId+" *** Not matched! ***")

				#print("Situation=",sitId)
				print(" - Things =", thingFlag)
				print(" - Objectives =", objectivesFlag)
				print(" - Localization =", locationFlag, " - '", distancelocation, "/", distLoc, "'meters")
				print(" - Date =", dateFlag, " - '", distanceDate, "/ Today", "'days")
				print(" - Time =", timeFlag, " - '", distanceTime, "/", distTime, "'minutes")

				if thingFlag and objectivesFlag and locationFlag and dateFlag and timeFlag:
					print("\n  --> Situation found! Setting strategy...")

					situationFound = True

					# Find strategy in the ontology
					triggerStrategy("sit_"+sitId)

				print("\n")

				###
				#checkCurrentStateWithObjectives(data)
				###

			
	# If a situation is not found, a new one is composed and sent to the entities
	if situationFound == False:
		print("\n -> Situation NOT found! Getting objectives and composing situation...")
		# Generate a new situation with the information collected in the ontology
		# 1. Analyze the objectives and match wih actions. Can they be solved with current actions?
		#TODO
		matchOjbectivesAction(data)

		# 2. Read the ontology and compose the situation (properties, objectives, strategy -with cur objectives and cur values-).
		#TODO

		# 2.1 Situation composition
		#TODO
		sit = getCurrentSituation(data)

		# 2.2 Send situation (to all entities presnt in the environment. Entities present in the environment are contained in presentThings.txt, by using the assignatted topic)
		sendSituation(sit)
		#TODO

		
	return


def triggerStrategy(strategy):
	print ("\n")
	print ("1/1. Trigger strategy")
	print ("---------------------")

	iri = onto.base_iri
	stra = iri+strategy
	print("STRATEGY=",stra)
	res = onto.search(iri = stra)
	res = str(res).replace("[","").replace("]","")
	res = res.split(".")[1]

	#print("RES=",res)

	# Get skills to be triggered
	properties = getObjectProperties(res, "hasStrategy")
	#print("PROPS=",properties)
	for p in properties:

		#print("P=",p)
		thing = p.itemThing
		thing=thing[0]
		sk = p.itemObjective
		sk=sk[0]
		skValue = p.itemValue
		skValue=skValue[0]

		#print(" - Thing=",thing)
		#print(" - Sk=",sk)
		#print(" - Value=",skValue)

		actions = onto[thing].hasAction

		for a in actions:

			ssk = str(a).split(".")[1]
			if ssk == sk:
				endp = a.forms
				#print(a)

				#print("Endpoint=",endp)

				endp = endp[0]
				#print("Ednp=",endp)

				#print ("Endp=",endp)

				try:
					endType = endp.split(";")[0]
					endContent = endp.split(";")[1]
					

					#print(endType)
					#print(endContent)
					#print(endpIp)
					
					a.realStateValue.append(skValue)
					onto.save()

					#act.realStateValue.append(skValue)

					if endType == "file":
						endpIp = endp.split(";")[2]
						cmd = endType+";"+endContent + " -ip " + endpIp + " -sk " + sk + " " + skValue
						#cmd = endp + " " + service + " " + sk_value
						#print ("EL CMD=",cmd)
						callEndpoint(str(cmd), "")
					elif endType == "subprocess":
						callEndpoint(str(endp), skValue)
					else:
						print ("\n -- Unknown endpoint")
				except:
					print ("\n -- Unknown endpoint =",endp)
		
	return



def matchOjbectivesAction(data):
	print ("\n")
	print ("1/3. Matching objectives and actions")
	print ("------------------------------------")

	presentEntitites = readPresentThings()

	e = data["id"]

	#print ("Entity=",e)
	#print ("Present entities=",presentEntitites)


	# Get ontology individuals
	individuals = list(onto.individuals())

	# Check entity objectives
	objectives = data["objectives"]
	#print ("Objectives=",objectives)

	for oo in objectives:
		for o in oo.items():
			#print (oo[0])
			
			objId = o[0]
			objValue = o[1]["value"]

			#print ("\nobjId=",objId)
			#print ("objValue=",objValue)

			# Find an associated action

			skFind = "*"+str(objId).replace("g_", "sk_")
			print ("Finding service for service = "+skFind)
			res = onto.search(iri = skFind)
			if res:
				print ("	-> Found service: ", res, " -> ", objId)
				for i in individuals:
					mod = onto.name+"."
					#iStr = str(i).split(str(mod))[1]
					#print ("iStr=",iStr)

					if str(type(i)) == str(onto.name)+".Entity":

						sk = onto.name+"."+skFind.replace("*","")
						#print ("SK="+sk)
						actions = i.hasAction
						#print (actions)

						aAux = Action(skFind.replace("*",""))
						#print("AAA=",aAux)

						if aAux in actions:
							print ("	-> Entity=", i)
							linkObjectiveAction(objId, str(objId).replace("g_", "sk_"))

							# Setting objective value
							print ("	-> Setthing objective value...")

							try:
								sk = str(objId).replace("g_", "sk_")
								#print("id=",skId)
								a = onto[sk]

								#print ("A=",a)

								endp = a.forms[0]
								#print("sk=",endp)

								#print("act=",i.hasAction)
								#print("act1=",i.hasAction[objId])

								print ("Endp=",endp)

								endType = endp.split(";")[0]
								#endContent = endp.split(";")[1]
								#endpIp = endp.split(";")[2]

								#print(endType)
								#print(endContent)
								#print(endpIp)

								

								skValue = objValue

								
								
								

								a.realStateValue.append(skValue)
								onto.save()

								#act.realStateValue.append(skValue)

								if endType == "file":

									endContent = endp.split(";")[1]
									endpIp = endp.split(";")[2]

									cmd = endType+";"+endContent + " -ip " + endpIp + " -sk " + sk + " " + skValue
									#cmd = endp + " " + service + " " + sk_value
									#print ("EL CMD=",cmd)
									callEndpoint(str(cmd), "")
								elif endType == "subprocess":							
									callEndpoint(str(endp), skValue)
								else:
									print (" - Unknown endpoint")		
							except:
								print (" - Unknown endpoint")











	

						



	"""
	if e in presentEntitites:
		print ("Present!")

	individuals = list(onto.individuals())
	for i in individuals:

		mod = onto.name+"."
		iStr = str(i).split(str(mod))[1]
		#print ("iStr=",iStr)

		#if str(type(i)) == str(onto.name)+".Objective":	
	"""

	#onto[eName]
	onto.save()

	return


def getCurrentSituation(data):
	print ("\n")
	print ("2/3. Getting current situation properties")
	print ("-----------------------------------------")


	curDate = datetime.datetime.now().date()
	curTime = datetime.datetime.now().strftime("%H:%M:%S")

	situation = {}

	# Main properties
	situation["id"] =  data["id"] + str(datetime.datetime.now().microsecond)
	situation["name"] =  data["id"]
	situation["properties"] = {}
	situation["properties"]["location"] = curLoc
	situation["properties"]["date"] = str(curDate)
	situation["properties"]["time"] = str(curTime)
	situation["properties"]["luminosity"] = 5
	situation["properties"]["weather"] = "sunny"

	# Objectives and Things
	situation["objectives"] = {}
	o = {}
	o["type"] = "array"
	o["items"] = []

	situation["things"] = {}
	t = {}
	t["type"] = "array"
	t["items"] = []

	situation["strategy"] = {}
	s = {}
	s["type"] = "array"
	s["items"] = []

	individuals = list(onto.individuals())
	for i in individuals:
		#mod = onto.name+"."
		#print ("iStr=",iStr)		
		#g = Operation()
		if str(type(i)) == str(onto.name)+".Objective":	
			a = onto.search (hasObjective = i)
			oT = str(a).replace("[","").replace("]","").split(".")[1]
			oO = str(i).split(".")[1]
			oV = i.powerValue[len(i.powerValue)-1]

			item = {}
			item["thing"] = oT
			item["objective"] = oO
			item["value"] = oV

			o["items"].append(item)

		if str(type(i)) == str(onto.name)+".Entity":	
			t["items"].append(str(i).split(".")[1])


		if str(type(i)) == str(onto.name)+".Action":
			#print("A=",i)

			item = {}
			

			ac = str(i).split(".")[1]
			#print ("AC=",ac)

			ac = Action(ac)

			thing = onto.search (hasAction = ac)
			#print ("t=",thing)

			if thing:
				# If the associated entity exists, we get its values
				item["thing"] = str(thing).replace("[","").replace("]","").split(".")[1]
				item["action"] = str(i).split(".")[1]

				# Search desired value
				des = str(item["action"]).replace("sk_","g_")
				#print("DES=",des)
				dv = onto[des]
		
				#print("DV=",dv)
				if dv:
					item["value"] = dv.powerValue[len(dv.powerValue)-1]
					#print("IV=",dv.powerValue[len(dv.powerValue)-1])
				else:
					item["value"] = onto.thing
				s["items"].append(item)
			else:
				# If not, get the values from the objectives
				thing = onto.search (hasAction = ac)
				#print (" - Thing empty")

			#print("ITEM=",item)

			


	#print("\nPAUSA\n")
	t["items"] = readPresentThings()
	situation["objectives"] = (o)
	situation["things"] = (t)
	situation["strategy"] = (s)

	print("\nSituation=",situation)

	# Add the situation to the ontology
	sitId = situation["id"]
	sit = Situation("sit_"+sitId)
	

	
	properties = situation["properties"]
	#print("p=",properties)
	for p in properties.items():
		#pr = p[1]
		
		propName = "sit_"+sitId+"-"+p[0]

		try:
			destroy_entity(onto[propName])
		except:
			pass

		sit = Situation("sit_"+sitId)

		prop = Property(propName)
		prop.realStateValue.append(p[1])
		sit.hasProperty.append(prop)

	# Situation objectives (items)

	

	objectives = situation["objectives"]["items"]
	for o in objectives:	

		sitObj = "sit_"+sitId+"_obj"+"-"+o["objective"]

		try:
			destroy_entity(onto[sitObj])
		except:
			pass

		it = Item(sitObj)
		it.itemThing.append(o["thing"])
		it.itemObjective.append(o["objective"])
		it.itemValue.append(o["value"])
		sit.hasObjective.append(it)
	
	# Situation things (entities)
	things = situation["things"]["items"]
	for t in things:
		e = Entity(t)
		sit.hasEntity.append(e)

	
	
	# Situation strategy
	sitStr = "sit_"+sitId+"_str"
	strategies = situation["strategy"]["items"]
	for s in strategies:	
		#print ("SSSS=",s)
		if s:
			sitStr = "sit_"+sitId+"_str"+"-"+s["action"]
		
			try:
				destroy_entity(onto[sitStr])
			except:
				pass

			it = Item(sitStr)
			it.itemThing.append(s["thing"])
			it.itemObjective.append(s["action"])
			
			if s["value"] != None:
				it.itemValue.append(s["value"])
			else:
				it.itemValue.append("none")
			
			sit.hasStrategy.append(it)


	# Add situation to the entities
	individuals = list(onto.individuals())
	for i in individuals:
		if str(type(i)) == str(onto.name)+".Entity":	
			eName = str(i).split(".")[1]
			onto[eName].hasSituation.append(sit)

	onto.save()

	return situation


def sendSituation(sit):
	print ("\n")
	print ("3/3. Sending situation to the entities")
	print ("--------------------------------------")
	# Send the situation to the entities

	#print("Situation="+str(sit))

	id = sit["id"]

	print ("id=",id)

	situation = {}
	situation[id] = sit

	cmd = "node pub.js -pub \"" + str(situation) +"\""

	print ("cmd="+cmd)

	if sendSituation == True:
		os.system("node pub.js -pub \"" + str(situation) +"\"") # Windows
		print("\nSituation sent!")
	else:
		print("\nSituation NOT sent! - Testing mode")
	
	return

def getEntityByObjective():
	entity = ""

	return entity

def getObjectProperties(ind, pro):
	properties = []
	for prop in onto[ind].get_properties():
		for value in prop[onto[ind]]:
			if prop.python_name == pro:
				if value not in properties:
					properties.append(value)
				#print(".%s == %s" % (prop.python_name, value))
	return properties


def matchOperationsServices():
	print ("\n")
	print ("Matching Operations and Services")
	print ("--------------------------------")
	individuals = list(onto.individuals())
	for i in individuals:

		mod = onto.name+"."
		iStr = str(i).split(str(mod))[1]
		#print ("iStr=",iStr)		
		#g = Operation()
		if str(type(i)) == str(onto.name)+".Operation":					
		
			# Find associate service
			iOperation = Operation(str(iStr))
			#print (iOperation.name)
			#s = str(iStr)
			#s = s.replace("g_", "sk_")
			#operation = Operation(s)
			#print ("finding=", s)
			#s = "*"+str(operation.name)
			#print (s)
			
			skFind = "*"+str(i.name).replace("g_", "sk_")
			print ("Finding service for operation = "+i.name)
			
			res = onto.search(iri = skFind)
			if res:
				print ("    -> Found service: ", res, " -> ", i.name)
				service = str(res).replace("[","").replace("]","").split(".")[1]
				#iService = Service(service)
				#iOperation = Operation(i.name)
				
				#print ("iOperation="+i.name)
				#print ("iService="+service)
								
				linkOperationService(i.name, service)
				
				# Setting value
				val = str(onto[i.name].powerValue).replace("[","").replace("]","").replace("'","")
				#print ("VAL=",val)
				#print ("VAL0=",onto[i.name])
				#print ("VAL1=",onto[i.name].powerValue)
				#print (val)
				
				#print (onto[service].hasAddress)
				sk_value = val
				endp = str(onto[service].hasAddress).replace("[","").replace("]","").replace("'","")
				endpIp= str(onto[service].IPAddress).replace("[","").replace("]","").replace("'","")
				endpID = str(onto[service].hasId).replace("[","").replace("]","").replace("'","")
				
				print ("id=",onto[service].hasId)
				
				#print (endp, service, sk_value)
				#print (sk_value)
				#cmd = endp + " " + service + " " + sk_value
				
				# Get endpoint type (file or process)
				endSplit = endp.split(";")
				endType = endSplit[0]
				endContent = endSplit[1]
				
				if endType == "file":
					cmd = endp + " -ip " + endpIp + " -id " + endpID + " -sk " + service + " " + sk_value
					#cmd = endp + " " + service + " " + sk_value
					print ("EL CMD=",cmd)
					callEndpoint(str(cmd), "")
				elif endType == "subprocess":
					callEndpoint(str(endp), sk_value)
				else:
					print ("Unknown endpoint")
				
				#print ("1-",endp)
				#print ("2-",service)
				#print ("3-",sk_value)
				
				
				
				#print ("	-> Executting...'"+str(cmd)+"'")
				
				#os.system(cmd)
				
				print ("")
			else:
				print ("    -> Service NOT found")
			
		#print (i)
		#print (" -> ", type(i))
	saveOntology()
	return
	

def showOntology():
	print ("\n")
	print ("Ontology data")
	print ("-------------")
	list(onto.classes())
	print (" - Classes: ", list(onto.classes()))
	print (" - Properties: ", list(onto.properties()))
	print (" - Data properties: ", list(onto.data_properties()))
	print (" - Individuals: ", list(onto.individuals()))
	print ("\n")
	
	#s =  "illuminance"
	#search (s)
	#searchSPARQL(s)
	
	return
	
def showOntologyRDFlib():
	return
	
def search(e):
	print ("\n")
	print ("Native search")
	print ("-------------")
	print ("Searching for '"+e+"' into the ontology...")
	res = onto.search(produces = e, _case_sensitive = "false")
	
	print (" --> Result =",res)
	
	return
	
def searchSPARQL(e):
	print ("\n")
	print ("SPARQL search")
	print ("-------------")
	print ("Searching for '"+e+"' into the ontology with SPARQL query...")
	my_world = World()
	onto = my_world.get_ontology(ontoName).load()
	graph = my_world.as_rdflib_graph()
	q = """ 
	PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
	PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
	PREFIX m3: <http://sensormeasurement.appspot.com/m3#> 
	PREFIX dc: <http://purl.org/dc/elements/1.1/>
	PREFIX owl: <http://www.w3.org/2002/07/owl#>
	
	SELECT DISTINCT ?entity ?subc ?p ?v
	WHERE {
	  ?entity rdf:type owl:Class.
	  ?entity rdfs:subClassOf* ?subc .
	  ?subc rdf:type owl:Restriction.
	  ?subc owl:onProperty ?p .
	  ?subc owl:someValuesFrom ?v .
	  FILTER REGEX (str(?p), 'produces','i') .
	  FILTER REGEX (str(?v), '"""+e+"""','i') . } order by ?entity """
	
	r = list(graph.query(q))
	i = 0
	print (" --> Result =", r)
	print ("\n --> Records found =", len(r))
	#for row in r:
	#	print(row)
	#	i=i+1
		
	#rs = str(r).split(",")
	#for i in rs:
	#	print (i)
		
	print ("\n --> Query =", q)
	return

def showOperations():
	print ("\n")
	print ("All goals")
	print ("---------")
	for ind in onto.individuals():
		#print (ind.get_properties())
		for prop in ind.get_properties():
			#print ("ind =",ind)
			#print (str(ind).split(".")[1])
			#e = "onto."+(str(ind).split(".")[1]);
			#print (e)
			for value in prop[ind]:
				try:
					if value.powerValue:
						eName = ' '.join(str(ind).split(".")[1:])
						eName = eName.replace(" ",".")
						print(eName,".%s == %s" % (prop.python_name, value) + " == ",  value.powerValue)
				except:
					pass
	return
	
def showServices():	
	print ("\n")
	print ("All skills")
	print ("---------")
	for ind in onto.individuals():
		#print (ind.get_properties())
		for prop in ind.get_properties():
			#print ("ind =",ind)
			#print (str(ind).split(".")[1])
			#e = "onto."+(str(ind).split(".")[1]);
			#print (e)
			for value in prop[ind]:
				try:
					if value.sk_CurValue:
						eName = ' '.join(str(ind).split(".")[1:])
						eName = eName.replace(" ",".")
						print(eName,".%s == %s" % (prop.python_name, value) + " == ",  value.sk_CurValue)
				except:
					pass
	return
	
def createService():
	print ("New service")
	print ("--------------")
	#ind = Service("Dani_luminosity_rgb")
	#print (ind.name)	
		
	return
	
def saveOntology():
	onto.save(file = ontoName, format = "rdfxml")
	print("\nOntology saved")
	return

def linkObjectiveAction(objective, action):
	print("		** Linking objective and action **")
	print("			", objective , " -> ", action)
	
	#g = Objective(objective)
	#sk = Action(action)
	
	gg = str(objective).split(onto.name+".")[0]
	#g = Objective(gg)
	g = onto[gg]
	
	sksk = str(action).split(onto.name+".")[0]
	#sk = Action(sksk)

	sk = onto[sksk]
	#print("SK=",sk)
	
	#print ("l_g=",g.name)
	#print ("l_sk=",sk.name)

	onto[gg].isImpactOf.append(sk)
	#onto[sksk].hasImpact.append(g)		

	
	#print ("1",onto[gg].isCoveredBy)
	#print ("2",onto[sksk].covers)
	
	#print ("------------LL-----------")
	#saveOntology()
	return
	
"""
def linkOperationService(operation, service):
	print("     ** Linking operation and service **")
	print("             ", operation , " -> ", service)
	
	g = Operation(operation)
	sk = Service(service)
	
	gg = str(g.name).split(onto.name+".")[0]
	g = Operation(gg)
	
	sksk = str(sk.name).split(onto.name+".")[0]
	sk = Service(sksk)
	
	#print ("l_g=",g.name)
	#print ("l_sk=",sk.name)
		
	onto[gg].isImpactOf.append(sk)
	#onto[sksk].covers.append(g)
	
	#print ("1",onto[gg].isCoveredBy)
	#print ("2",onto[sksk].covers)
	
	#print ("------------LL-----------")
	#saveOntology()
	return
"""
	
def callEndpoint(endpoint, value): # Value is optional
	print ("	-- Calling endpoint...")
	ep = endpoint.split(";")
	type = ep[0]
	endp = ep[1]
	#print (type)
	#print (endp)
	if type == "file":
		# Executting endpoint trought script
		print ("\n -- Executting endpoint trought script")
		try:
			endp = "./scripts/"+endp
			print ("	-> Executting script: ", endp)
			#os.system("python3 "+endp) # Raspi
			os.system("python "+endp) # Windows
			print ("	-> Endpoint executed succesfully!")
		except:
			print ("	-> Endpoint execution error!\n")
		
	elif type == "subprocess":
		# Executtion endpoint as a process
		print (" -- Executting endpoint trought subprocess")
		try:
			print ("	-> Executting process: ", endp, value)
			#subprocess.run([endp],value) # Windows
			fendp = endp + " " +str(value) # Raspi
			os.system(fendp) # Raspi
			print ("	-> Endpoint executed succesfully!")
		except:
			print ("	-> Endpoint execution error!\n")
		
	else:
		print ("	-> Unknown endpoint\n")
	return


def executionLog():
	global logData
	global end
	global start
	end = time.time()
	executionTime = end - start
	
	log = str('Execution recorded at %s;' % str((datetime.datetime.now())).rstrip("\n") + str(logData).rstrip('\r\n') + ";" + str(executionTime).rstrip('\r\n')).replace('\n', ' ').replace('\r', '') + "\n"
	
	with open("log.txt", mode='a') as file:
		file.write(log)
	return

def readPresentThings():
	#print ("\n")
	#print ("Present entities in the environment")
	#print ("-------------")
	file = open("presentThings.txt", "r")
	data = json.load(file)
	presentThings = []
	for e in data.items():
		if (e[1])==1:
			presentThings.append(e[0])

	return presentThings

def editPresentThings(entity, status):
	#print ("\n")
	#print ("Storing entity state:",entity,"=",1)
	#print ("-------------")

	# Check if file exists
	try:
		file = open("presentThings.txt")
		# Read file
		#print("File accessible")
		data = json.load(file)
		data[entity] = status
		with open('presentThings.txt', 'w') as outfile:
			json.dump(data, outfile)			

	except IOError:
		#print("File NOT accessible")
		# Create file
		data = {}
		data[entity] = status
		with open('presentThings.txt', 'w') as outfile:
			json.dump(data, outfile)

	return


	
#loadOntology()
#showOntology()

#parseJSON("./config-files/daniel.cfg")
#parseJSON("./config-files/yeelight.cfg")

#loadOntologyRDFlib()
#showOntologyRDFlib()

#parseYaml()
#saveOntology()
#showOntology()

#showServices()
#showOperations()

#matchOperationsServices()
#saveOntology()

def main():
	if len(sys.argv) > 1:
		mode = sys.argv[1]	
		try:
		
			global start
			start = time.time()
		
			# Load Json entity config file
			if mode == "-config" or mode == "-c":		
				fileName = sys.argv[2]
				try:
					sendSituation = sys.argv[3]
				except:
					sendSituation =  True
				loadOntology()
				data = parseJSON(fileName)
				#W3CTD-EXT
				#checkEntitySituationWithObjectives(data) # TEST
				#checkCurrentStateWithObjectives(data) # TEST
				checkSituations(data) # Descomentar esta linea!!!!!!!!
				#matchOperationsServices()
			# Load Yaml entity config file
			elif mode == "-configy" or mode == "-cy":
				fileName = sys.argv[2]
				loadOntology()
				parseYaml(fileName)
			# Clear the ontology
			elif mode == "--clear":
				# clearOntology()
				pass
			elif mode == "-m" or mode == "--match":
				loadOntology()
				matchOperationsServices()
			elif mode == "--help" or mode == "-h":
				print ("Help:\n\n -c || --config [fileNameJson]: load the file (Json) into the ontology\n -cy || --configy [fileNameYaml]: load the file (Yaml) into the ontology\n -m || --match: match Operations and Services (Goals and Skills) in the current ontology\n - ")
			else:
				print ("\n--- Invalid params\n")
			executionLog()
		except Exception as e:
			print(e)
			print ("\n-- An error occured:",e)
			print ("\n -- Ending...")

	else:
		print ("\n--- Not arguments provided. Showing the ontology\n")
		loadOntology()
		showOntology()
		
	print ("\n-- End\n")
	
if __name__== "__main__":
  main()



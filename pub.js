'use strict';
const fs = require('fs');
var dateTime = require('node-datetime');

function main() {
    // SERVER
    var myArgs = process.argv.slice(2);
    console.log('myArgs: ', myArgs);

    switch (myArgs[0]) {
    case '-pub':
        var mqtt=require('mqtt');
        var client = mqtt.connect("mqtt://localhost:1883");
        client.on("connect",function(){
            var msg =  myArgs[1];
            if (typeof msg === "undefined"){
                console.log("Sending default situation...");
                var msg = "{'home': {'id': 'home','name': 'Caceres flat','properties': {'location': '39.4570601,-6.3835215','date': '2012-12-03','time': '15:05:00','luminosity': '7','weather': 'sunny'},'objectives': {'type': 'array','items': [{'thing': 'W3CTDE-Daniel','objective': 'g_sh_switch','value': 'off'}]},'things': {'type': 'array','items': ['W3CTDE-Daniel']},'strategy': {'type': 'array','items': [{'thing': 'W3CTDE-Shelly10x001','action': 'sk_sh_switch','value': 'on'}]}}}"
            }

            // Publish situation
            client.publish("situation",msg);

            console.log("Publishing =",msg);	  
            client.end()
            
        });
        break;
    default:
        console.log('-pub + message');
    }
    // END SERVER

}

if (require.main === module) {
    main();
}

